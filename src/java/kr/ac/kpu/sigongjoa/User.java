package kr.ac.kpu.sigongjoa;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class User {
	private int userid;
	private String domain;
	private String id;
	private String passwd;
	private String nickname;
	private String profile;
	private List<String> followers;
	private List<String> followings;
	private List<Content> timeline;
	private String path;
	
	public User() {}

	public User(String domain) {
		setDomain(domain);
	}

	public int loadUser() {
		return
			loadPasswd() |
			loadNickname() << 1 |
			loadProfile() << 2 |
			loadFollowers() << 3 |
			loadFollowings() << 4 |
			loadTimeline() << 5;
	}

	public int loadUser(int uid) {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "__userlist__"))) {
			userid = uid;
			String buf;
			for(int line = 1; (buf = in.readLine()) != null; ++line) {
				if(line == uid) {
					id = buf.trim();
					return loadUser();
				}
			}

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadUser(String id) {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "__userlist__"))) {
			this.id = id;
			String buf;
			for(int line = 1; (buf = in.readLine()) != null; ++line) {
				if(buf.equals(id)) {
					id = buf.trim();
					return loadUser();
				}
			}

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadPasswd() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + id + "/passwd"))) {
			passwd = in.readLine().trim();
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadNickname() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + id + "/nickname"))) {
			nickname = in.readLine().trim();
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadProfile() { //
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + id + "/profile"))) {
			profile = in.readLine().trim();
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadFollowers() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + id + "/followers"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				followers.add(buf.trim());
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadFollowings() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + id + "/followings"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				followings.add(buf.trim());
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadTimeline() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + id + "/timeline"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				buf = buf.trim();
				
				if(buf.startsWith("&")) {
					String domainBuf = buf.substring(1, buf.indexOf(':'));
					String titleBuf = buf.substring(buf.indexOf(':') + 1);
					Content c = new Post(domainBuf);
					c.loadContent(titleBuf);
					timeline.add(c);
				} else {
					Content c = new Post(domain);
					c.loadContent(buf);
					timeline.add(c);
				}
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveUser() {
		return
			savePasswd() |
			saveNickname() << 1 |
			saveProfile() << 2 |
			saveFollowers() << 3 |
			saveFollowings() << 4 |
			saveTimeline() << 5;
	}

	public int saveUser(String domain) {
		setDomain(domain);
		return saveUser();
	}

	public int savePasswd() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + id + "/passwd"), StandardOpenOption.CREATE))) {
			out.println(passwd);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveNickname() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + id + "/nickname"), StandardOpenOption.CREATE))) {
			out.println(nickname);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveProfile() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + id + "/profile"), StandardOpenOption.CREATE))) {
			out.println(profile);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveFollowers() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + id + "/followers"), StandardOpenOption.CREATE))) {
			for(String follower : followers) {
				out.println(follower);
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveFollowings() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + id + "/followings"), StandardOpenOption.CREATE))) {
			for(String following : followings) {
				out.println(following);
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveTimeline() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + id + "/timeline"), StandardOpenOption.CREATE))) {
			for(Content c : timeline) {
				if(c.getDomain().equals(domain)) {
					out.println(c.getTitle());
				} else {
					out.println("&" + c.getDomain() + ":" + c.getTitle());
				}
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int checkPasswd(String input) {
		return passwd.compareTo(input);
	}

	public void addFollower(String id) {
		followers.add(id);
	}

	public void addFollowing(String id) {
		followings.add(id);
	}

	public void addTimeline(String title) {
		Content c = new Content(domain);
		c.loadContent(title);
		addTimeline(c);
	}

	public void addTimeline(Content c) {
		timeline.add(c);
	}

	public void setPath(String domain) {
		path = "servers/" + domain + "/database/users/";
	}

	public int getUid() {
		return userid;
	}

	public void setUid(int userid) {
		this.userid = userid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
		setPath(domain);
	}

	public List<String> getFollowers() {
		return followers;
	}

	public void setFollowers(List<String> followers) {
		this.followers = followers;
	}

	public List<String> getFollowings() {
		return followings;
	}

	public void setFollowings(List<String> followings) {
		this.followings = followings;
	}

	public List<Content> getTimeline() {
		return timeline;
	}

	public void setTimeline(List<Content> timeline) {
		this.timeline = timeline;
	}
}
