package kr.ac.kpu.sigongjoa;

public interface Client {
	void run();
	int connect();
	int disconnect();
	int help();
}
